package GenericsOne;

public class TestGeneric {

    public static void main(String[] args) {


        Generic<String> numeAngajat = new Generic<String>("Samuel Jackson");
        Generic<String> pozitieAngajat = new Generic<String>("cashier");
        Generic<Integer> salariuAngajat = new Generic<Integer>(2000);



        System.out.println("My name is " + numeAngajat.get() + ".");
        System.out.println("I work as " +  pozitieAngajat.get() + " at Plaza Hotel.");
        System.out.println("My salary is valued somewhere around " + salariuAngajat.get() + " dollars.");


    }
}
